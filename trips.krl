ruleset track_trips {
  meta {
  name "Hello World"
  author "Ken Reese"
  logging on
  }
  
  rule process_trip {
    select when echo message
    pre {
      mileage = event:attr("mileage")
    }
    send_directive("trip") with
      trip_length = mileage
  }

}