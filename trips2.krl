ruleset track_trips_2 {
  meta {
    name "Track Tripper 2"
    author "Ken Reese"
    logging on
  }

  global {
      long_trip = 500
  }

  rule process_trip {
      select when car new_trip
      pre {
          mileage = event:attr("mileage").klog("Just got mileage of: ")
          timestamp = time:now()
      }
      fired {
          raise explicit event "trip_processed"
            attributes {
                "mileage": mileage,
                "timestamp": timestamp
            }
      }
  }

  rule find_long_trips {
    select when explicit trip_processed
    pre {
      mileage = event:attr("mileage").as("Number")
      timestamp = event:attr("timestamp")
    }

    if (long_trip < mileage) then noop()
    fired {
      raise explicit event "found_long_trip"
        attributes {
            "timestamp": timestamp,
            "mileage": mileage
        }
    }
  }

  rule note_long_trip {
      select when explicit found_long_trip
      pre {
          new_record = event:attr("mileage").klog("Just got a long trip of: ")
      }
      send_directive("notice a long trip") with
        long_trip = new_record

  }

}