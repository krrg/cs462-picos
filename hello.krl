ruleset hello_world {
  meta {
  name "Hello World"
  description <<
A first ruleset for the Quickstart
>>
  author "Ken Reese"
  logging on
  shares hello, __testing
  }
  
  global {
  hello = function(obj) {
    msg = "Hello " + obj;
    msg
  }

  __testing = { "queries": [ { "name": "hello", "args": [ "obj" ] },
          { "name": "__testing" } ],
    "events": [ { "domain": "echo", "type": "hello" } ]
  }
  }
  
  rule hello_world {
    select when echo hello
    send_directive("say") with
      something = "Hello World"
  }

  rule hello_message {
  select when echo message 
  pre {
    input = event:attr("input")
  }
  send_directive("say") with
    something = input
  }

}
  
