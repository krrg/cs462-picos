ruleset trip_store {
    meta {
        name "Trip Storage"
        author "Ken Reese"
        logging on
        provides trips, long_trips, short_trips
        shares trips, long_trips, short_trips
    }

    global {
        trips = function() {
            ent:trips
        }

        long_trips = function() {
            ent:longTrips
        }

        short_trips = function() {
            ent:trips.difference(ent:longTrips)
        }
    }

    rule collect_trips {
        select when explicit trip_processed
        pre {
            mileage = event:attr("mileage").as("Number")
            timestamp = event:attr("timestamp")
        }
        fired {
            ent:trips := ent:trips.append({
                "mileage": mileage,
                "timestamp": timestamp
            })
        }
    }

    rule collect_long_trips {
        select when explicit found_long_trip
        pre {
            mileage = event:attr("mileage").as("Number")
            timestamp = event:attr("timestamp")
        }
        fired {
            ent:longTrips := ent:longTrips.append({
                "mileage": mileage,
                "timestamp": timestamp
            })
        }
    }

    rule clear_trips {
        select when car trip_reset
        send_directive("okay")
        fired {
            ent:trips := [];
            ent:longTrips := []
        }
    }

}